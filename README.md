# Webpack 分享

## 本次分享大纲

1. `Webpack` 简介
2. `Webpack ^4.0.0` 与之前版本的区别
3. `Webpack` 常见的性能优化
4. `Vue cli ^3.0.0` 中的实践

 现在的 Web 应用经常会使用 📦 工具来将其依赖的静态文件构建成压缩过 `production bundle` , 来给用户带来更流畅的体验. 这里我将大致介绍下如何用 `Webpack` 来更高效的处理我们的静态资源, 让我们的 Web 应用拥有更高的性能...

### 1. Webpack 简介

> 什么是 `Webpack`? 它能给我们带来什么?

`Webpack` 是目前最流行的打包工具之一, 我们可以用它来优化我们现有的代码, 将代码分割成核心部分和非核心部分并剔除其中的无用代码, 来确保我们的 Web 应用具有最小的网络成本和处理成本.

![](https://blog-crown-im-1256172570.file.myqcloud.com/dq7js.jpg)

---

### 2. `Webpack 4` 与之前版本的区别

在 `Webpack ^4.0.0` 到来之前, webpack 最被人诟病的一点就是配置极其繁琐, 任何和打包相关的配置都需要自己去其 `config.js` 里面去具体的配置, 这个时候社区有人发起了一个叫 [📦Parcel](https://parceljs.org/) 的项目, 也是用来做 Web 应用的打包构建的, 不过它比 Webpack 强在它可以做到开箱即用, 默认配置对开发人员十分 👬

这个时候 `Webpack` 受到这个项目的冲击, 在一个月左右的时间就马上推出了 `4.0.0-alpha` 版, 在这个新版本里面将增加了大量的默认配置, 以及打包上的性能提升(我怀疑就是默认配置带来的效果).

举个很简单的 🌰, 你们感受下 3 和 4 的区别:

在我们的一些常见的依赖库里面有很多代码是专门用在开发环境的, 例如 `Vue`

```js
// vue/dist/vue.runtime.esm.js
if (typeof val === 'string') {
  name = camelize(val)
  res[name] = { type: null }
} else if (process.env.NODE_ENV !== 'production') {
  warn('props must be strings when using array syntax.')
}
```

而我们要在生产环境中移除这些代码, 我们需要在 `Webpack` 中进行相应的配置:

```js
// webpack.config.js (for webpack 3)
const webpack = require('webpack')

module.exports = {
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"production"',
    }),
    new webpack.optimize.UglifyJsPlugin(),
  ],
}
```

> 注意: 在 webpack 3 中, webpack 内置的 `UglifyJS` 插件不能编译 ES6 代码. 如果你需要编译你的 ES6 代码的话, 你需要额外安装一个叫 [uglifyjs-webpack-plugin ](https://github.com/webpack-contrib/uglifyjs-webpack-plugin) 的插件.

而 **webpack^4.0.0** 中我们只需要

```js
// webpack.config.js (for webpack 4)
module.exports = {
  optimization: {
    nodeEnv: 'production',
    minimize: true,
  },
}
```

---

### 3. `Webpack` 常见的性能优化

这里主要分为三个部分:

1. 减小静态文件的大小
2. 利用好长期缓存
3. 监控并分析你的 Web 应用文件

#### 3.1 减小静态文件的大小

##### 3.1.1 使用 `production mode` (webpack 4 才支持)

就像我们上面讲的那个例子一样

##### 3.1.2 启动 `minification`

`minification` 会在你压缩代码时移除其中多余的空格, 缩短你的变量命名等工作. 例如:

```js
// Original code
function map(array, iteratee) {
  let index = -1
  const length = array == null ? 0 : array.length
  const result = new Array(length)

  while (++index < length) {
    result[index] = iteratee(array[index], index, array)
  }
  return result
}
```

<!-- prettier-ignore -->
```js
// Minified code
function map(n,r){let t=-1;for(const a=null==n?0:n.length,l=Array(a);++t<a;)l[t]=r(n[t],t,n);return l}
```

`webpack` 支持两种方式来最小化你的代码: `the bundle-level minification` && `loader-specific options`.

> 这一块的内容大部分和 Webpack 3 相关. 如果你已经在 Webpack 4 中 启用了 `production mode`, `the bundle-level minification` 已经默认开启了, 你只需要去调整你的 `loader-specific options`即可.

###### Bundle-level minification

`Bundle-level minification` 会在你的代码编译后去压缩整个 `bundle`.

1. 例如这是你的代码:

```js
// comments.js
import './comments.css'
export function render(data, target) {
  console.log('Rendered!')
}
```

2. Webpack 会将其编译成下面差不多的样子

<!-- prettier-ignore -->
```js
// bundle.js (part of)
"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["render"] = render;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__comments_css__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__comments_css_js___default =
__webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__comments_css__);

function render(data, target) {
  console.log('Rendered!');
}
```

3. `minifier` 会压缩将其压缩成下面的样子

<!-- prettier-ignore -->
```js
// minified bundle.js (part of)
"use strict";function t(e,n){console.log("Rendered!")}
Object.defineProperty(n,"__esModule",{value:!0}),n.render=t;var o=r(1);r.n(o)
```

###### Loader-specific options

使用 `loader` , 你可以压缩一些 `minifier` 不能处理的东西. 例如, 当你使用 `css-loader` 来处理你引入的 css 文件时, 它会先被编译成 string:

```css
/* comments.css */
.comment {
  color: black;
}
```

⬇️

<!-- prettier-ignore -->
```js
// minified bundle.js (part of)
exports=module.exports=__webpack_require__(1)(),
exports.push([module.i,".comment {\r\n  color: black;\r\n}",""]);
```

`minifier`是不能处理这种 string 的. 这时为了压缩这种文件内容, 我们就需要配置特殊的 `loader` 来处理它:

```js
// webpack.config.js
module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader', options: { minimize: true } },
        ],
      },
    ],
  },
}
```

##### 3.1.3 使用 ES modules

当你在代码中使用 ES modiles 时, webpack 能据此来做 `tree-shaking`. `Tree-shaking` 是指你的 `bundler` 在打包时会去遍历整个相关的依赖关系树, 检查哪些依赖是使用过的, 并移除那些没被使用过的. 因此, 当你使用 ES module 的语法时, webpack 能移除那些无用的代码.

1. 当你写了一个含有多个导出的 js 文件, 但你的 app 却只使用了其中的一个

```js
// comments.js
export const render = () => {
  return 'Rendered!'
}
export const commentRestEndpoint = '/rest/comments'

// index.js
import { render } from './comments.js'
render()
```

2. webpack 能解析 `commentRestRndpoint` 是没有使用过的并且不会为它生成一个导出点

```js
// bundle.js (part that corresponds to comments.js)
;(function(module, __webpack_exports__, __webpack_require__) {
  'use strict'
  const render = () => {
    return 'Rendered!'
  }
  /* harmony export (immutable) */ __webpack_exports__['a'] = render

  const commentRestEndpoint = '/rest/comments'
  /* unused harmony export commentRestEndpoint */
})
```

3. 最后 `minifier` 会移除这个无用变量:

<!-- prettier-ignore -->
```js
// bundle.js (part that corresponds to comments.js)
(function(n,e){"use strict";var r=function(){return"Rendered!"};e.b=r})
```

如果你引用的其它库也是拿 `ES Module` 写的, 上述的流程也会其作用.

> 这里需要注意的是, tree-shaking 只有和 minifier 配合时才会起作用. webpack 只会移除那些没有使用过的 export, 接下来 minifier 才会去移除那些无用的代码. 这里你可以使用任何支持 `dead code removal` 的 `minifier`, 例如[Babel Minify plugin](https://github.com/webpack-contrib/babel-minify-webpack-plugin) or [Google Closure Compiler plugin](https://github.com/roman01la/webpack-closure-compiler))
> 还有, 记得不要把你的 ES modile 编译成 CommonJS
> 如果你使用 Babel 的 `babel-preset-env` 或者 `babel-preset-es2015`, 记得检查下它的设置. 它们默认会把 ES `import` 和 `export` 转换成 CommonJS `require` 和 `module.exports`. 通过[设置 `{ modules: false }`](https://github.com/babel/babel/tree/master/packages/babel-preset-env) 来禁用这种行为
> 同理还有 TypeScript: 记得在你的 `tsconfig.json` 中设置 `{ "compilerOptions": { "module": "es2015" } }`

##### 3.1.4 压缩图片

图片一般会占用我们页面的一半大小以上. 虽然它们没有像 JS 那么重要(例如, 它们不会阻塞页面的渲染), 但它们仍然会占用和大的带宽. 在 webpack 里, 我们可以使用 `url-loader`, `svg-url-loader` 和 `image-webpack-loader` 来压缩它们.

`url-loader` 能将那些小的静态文件内联到 app 里面去. 当我们对它不做特殊设置时, 它默认会把文件丢给打包工具处理并返回对应的 url. 但是但我们指定了 `limit` 配置时, 它会对满足条件的文件进行 `base64` 处理, 并内联进我们的代码中, 来减少一次 http 请求.

```js
// webpack.config.js
module.exports = {
  module: {
    rules: [
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'url-loader',
        options: {
          // Inline files smaller than 10 kB (10240 bytes)
          limit: 10 * 1024,
        },
      },
    ],
  },
}
```

```js
// index.js
import imageUrl from './image.png'
// → If image.png is smaller than 10 kB, `imageUrl` will include
// the encoded image: 'data:image/png;base64,iVBORw0KGg…'
// → If image.png is larger than 10 kB, the loader will create a new file,
// and `imageUrl` will include its url: `/2fcd56a1920be.png`
```

`svg-url-loader` 工作机制跟 `url-loader` 差不多, 不过它是使用 `URL encoding` 而不是 `Base64`. 这种机制更适合于 SVG, 因为 svg 本质上只是一段纯文本, 使用 `URL encoding` 会有更好的效果.

```js
// webpack.config.js
module.exports = {
  module: {
    rules: [
      {
        test: /\.svg$/,
        loader: 'svg-url-loader',
        options: {
          // Inline files smaller than 10 kB (10240 bytes)
          limit: 10 * 1024,
          // Remove the quotes from the url
          // (they’re unnecessary in most cases)
          noquotes: true,
        },
      },
    ],
  },
}
```

`image-webpack-loader` 可以压缩图片, 它支持 JPG, PNG, GIF && SVG. 由于它只做压缩的工作, 并不会将我们的图片嵌入到对应的代码里面去, 所以它必须配合 `url-loader` 和 `svg-url-loader` 使用, 这里为了避免在上面两个 loader 的配置文件里面写重复的代码, 我们通过设置 `enforce: 'pre'` 来让它前置并强制执行:

```js
// webpack.config.js
module.exports = {
  module: {
    rules: [
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        loader: 'image-webpack-loader',
        // This will apply the loader before the other ones
        enforce: 'pre',
      },
    ],
  },
}
```

##### 3.1.5 压缩依赖库

我们项目里面几乎超过一半的大小来自于 各种依赖库, 而这些依赖库里面很大的一部分代码我们是完全用不上.

例如 Lodash(v4.17.4) 压缩过的代码差不多有 72 KB, 但是假设你只使用到了其中的 20 种 `method` , 那么其中接近 65 KB 左右的代码对你的项目是完全没用的.

再举一个 🌰: Monment.js(v2.19.1) 压缩过的代码差不多有 223 KB, 然而其中 170 KB 左右的文件是跟 地域化相关的. 如果你不需要那么多种语言支持的话, 这些文件你是完全可以 cut 的.

这里有一些常见库的[压缩技巧总结](https://github.com/GoogleChromeLabs/webpack-libs-optimizations)(Google 收集的)

![](https://blog-crown-im-1256172570.file.myqcloud.com/l9dok.jpg)

##### 3.1.6 为 ES module 启用模块级联(又名 作用域提升)

> webpack 4 production mode 已经默认启用了

当你在构建一个 bundle 时, webpack 会将每个模块丢到一个函数里面去

```js
// index.js
import { render } from './comments.js'
render()

// comments.js
export function render(data, target) {
  console.log('Rendered!')
}
```

⬇️

```js
// bundle.js (part  of)
/* 0 */
;(function(module, __webpack_exports__, __webpack_require__) {
  'use strict'
  Object.defineProperty(__webpack_exports__, '__esModule', { value: true })
  var __WEBPACK_IMPORTED_MODULE_0__comments_js__ = __webpack_require__(1)
  Object(__WEBPACK_IMPORTED_MODULE_0__comments_js__['a' /* render */])()
},
  /* 1 */
  function(module, __webpack_exports__, __webpack_require__) {
    'use strict'
    __webpack_exports__['a'] = render
    function render(data, target) {
      console.log('Rendered!')
    }
  })
```

在以前, 这是为了隔离不同的 `CommonJS/AMD module`. 然而, 这么做会增加打包的大小和不同模块之间的性能开销.

而 webpack 的模块级联会进行如下处理:

```js
// index.js
import { render } from './comments.js'
render()

// comments.js
export function render(data, target) {
  console.log('Rendered!')
}
```

⬇️

```js
// Unlike the previous snippet, this bundle has only one module
// which includes the code from both files

// bundle.js (part of; compiled with ModuleConcatenationPlugin)
/* 0 */
;(function(module, __webpack_exports__, __webpack_require__) {
  'use strict'
  Object.defineProperty(__webpack_exports__, '__esModule', { value: true })

  // CONCATENATED MODULE: ./comments.js
  function render(data, target) {
    console.log('Rendered!')
  }

  // CONCATENATED MODULE: ./index.js
  render()
})
```

在之前的打包方式中, module 0 依赖于 module 1 的 `render`. 在使用模块级联后, `require`被 required function 取代, module 1 也被移除. 我们的 bundle 也因此拥有了 更少的 module 和其附带的负载.

```js
// webpack.config.js (for webpack 4)
module.exports = {
  optimization: {
    concatenateModules: true,
  },
}

// webpack.config.js (for webpack 3)
const webpack = require('webpack')

module.exports = {
  plugins: [new webpack.optimize.ModuleConcatenationPlugin()],
}
```

#### 3.2 利用好长期缓存

##### 使用 bundle versioning 和 cache headers

```js
# Server header
Cache-Control: max-age=**31536000**

// webpack.config.js
module.exports = {
  entry: './index.js',
  output: {
    filename: 'bundle.[chunkhash].js',
        // → bundle.8e0d62a03.js
  },
};
```

##### 把你的 dependencies 和 runtime 放到不同的文件里面

> Dependencies

App 的依赖项一般变动的比较少, 如果你把它们放到单独的文件里面去, 浏览器能更好地缓存它们, 而不是在你每次 app 代码变动时都去把它们重新下载一遍.

为了达到这种效果, 我们只需要做三件事就行:

1. 把我们的 output filname 替换成 `[name].[chunkname].js`:

```js
// webpack.config.js
module.exports = {
  output: {
    // Before
    filename: 'bundle.[chunkhash].js',
    // After
    filename: '[name].[chunkhash].js',
  },
}
```

2. 把 `entry` 字段换成一个对象:

```js
// webpack.config.js
module.exports = {
  // Before
  entry: './index.js',
  // After
  entry: {
    main: './index.js',
  },
}
```

3. 在 webpack 4 中增加 `optimization.splitChunks.chunk: 'all'` 到你的配置中去:

```js
// webpack.config.js (for webpack 4)
module.exports = {
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
}
```

这项配置会启用更小的代码切割. 通过它, webpack 将会那些超过 30 KB 的 分离到 vender 里面去( 压缩和 gzip 之前).

在 webpack 3 中, 你需要增加 `CommonsChunkPlugin`:

```js
// webpack.config.js (for webpack 3)
module.exports = {
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      // A name of the chunk that will include the dependencies.
      // This name is substituted in place of [name] from step 1
      name: 'vendor',

      // A function that determines which modules to include into this chunk
      minChunks: module =>
        module.context && module.context.includes('node_modules'),
    }),
  ],
}
```

```bash
$ webpack
Hash: ac01483e8fec1fa70676
Version: webpack 3.8.1
Time: 3816ms
                           Asset   Size  Chunks             Chunk Names
  ./main.00bab6fd3100008a42b0.js  82 kB       0  [emitted]  main
./vendor.d9e134771799ecdf9483.js  47 kB       1  [emitted]  vendor
```

> Runtime

不幸的是, 如果只是分离我们的 vender code 还是不够的, 如果你在你的 app 代码随便改动了什么:

```js
// index.js
…
…

// E.g. add this:
console.log('Wat');
```

你的 vender hash 也会随之改变:

![](https://blog-crown-im-1256172570.file.myqcloud.com/hri7z.jpg)

会发生这种情况是因为我们的 vender 里面包含了 webpack 专门用来管理不同模块之间执行与映射的 `runtime code`

```js
// vendor.e6ea4504d61a1cc1c60b.js
script.src =
  __webpack_require__.p +
  chunkId +
  '.' +
  {
    '0': '2f2269c7f0a55a5c1871',
  }[chunkId] +
  '.js'
```

webpack 会把 runtime 丢带最后生成的 chunk 中, 这也是为什么它会在 vender 里面的原因. 而我们每次修改代码时, runtime 是一定会改变的, 这也导致了 vender 也会随之改变.

为了解决这个问题, 我们最好将 runtime 拆分到一个单独的文件里面去. 在 webpack 4 中, 我们只需要启用 `optimization.runtimeChunk` 即可:

```js
// webpack.config.js (for webpack 4)
module.exports = {
  optimization: {
    runtimeChunk: true,
  },
}
```

在 webpack 3 中, 我们还是需要使用到 `CommonsChunkPlugin`:

```js
// webpack.config.js (for webpack 3)
module.exports = {
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',

      minChunks: module =>
        module.context && module.context.includes('node_modules'),
    }),

    // This plugin must come after the vendor one (because webpack
    // includes runtime into the last chunk)
    new webpack.optimize.CommonsChunkPlugin({
      name: 'runtime',

      // minChunks: Infinity means that no app modules
      // will be included into this chunk
      minChunks: Infinity,
    }),
  ],
}
```

在坐了上述变动后, 我们的打包结果会变成下面这个样子:

```bash
$ webpack
Hash: ac01483e8fec1fa70676
Version: webpack 3.8.1
Time: 3816ms
                            Asset     Size  Chunks             Chunk Names
   ./main.00bab6fd3100008a42b0.js    82 kB       0  [emitted]  main
 ./vendor.26886caf15818fa82dfa.js    46 kB       1  [emitted]  vendor
./runtime.79f17c27b335abc7aaf4.js  1.45 kB       3  [emitted]  runtime
```

##### 把 runtime code 内置到 html 里来节省一次 HTTP 请求

```html
<!-- index.html -->
<script src="./runtime.79f17c27b335abc7aaf4.js"></script>

<!-- index.html -->
<script>
  !function(e){function n(r){if(t[r])return t[r].exports;…}} ([]);
</script>
```

> 如果你用 HtmlWebpackPlugin 来生成 HTML

```js
// webpack.config.js
const HtmlWebpackPlugin = require('html-webpack-plugin')
const InlineSourcePlugin = require('html-webpack-inline-source-plugin')

module.exports = {
  plugins: [
    new HtmlWebpackPlugin({
      // Inline all files which names start with “runtime~” and end with “.js”.
      // That’s the default naming of runtime chunks
      inlineSource: 'runtime~.+\\.js',
    }),
    // This plugin enables the “inlineSource” option
    new InlineSourcePlugin(),
  ],
}
```

> 如果你使用服务端渲染来生成 HTML

1. 添加 `WebpackManifestPlugin`

```js
// webpack.config.js (for webpack 4)
const ManifestPlugin = require('webpack-manifest-plugin')

module.exports = {
  plugins: [new ManifestPlugin()],
}
```

⬇️

```json
// manifest.json
{
  "runtime~main.js": "runtime~main.8e0d62a03.js"
}
```

2. 将 runtime chunk 的内容内置到 HTML 模版里. 例如使用 `Node` && `Express`

```js
// server.js
const fs = require('fs')
const manifest = require('./manifest.json')

const runtimeContent = fs.readFileSync(manifest['runtime~main.js'], 'utf-8')

app.get('/', (req, res) => {
  res.send(`
    …
    <script>${runtimeContent}</script>
    …
  `)
})
```

##### Lazy-load 那些你不是立刻需要用到的代码

```js
// videoPlayer.js
export function renderVideoPlayer() { … }

// comments.js
export function renderComments() { … }

// index.js
import {renderVideoPlayer} from './videoPlayer';
renderVideoPlayer();

// …Custom event listener
onShowCommentsClick(() => {
  import('./comments').then((comments) => {
    comments.renderComments();
  });
});
```

##### 按路由和页面来切割你的代码

##### 让你的 module ids 更加稳固

module id 默认是按递增计算而来的, 这种顺序是非常容易被打破的.

```bash
$ webpack
Hash: df3474e4f76528e3bbc9
Version: webpack 3.8.1
Time: 2150ms
                           Asset      Size  Chunks             Chunk Names
      ./0.5c82c0f337fcb22672b5.js    22 kB       0  [emitted]
   ./main.0c8b617dfc40c2827ae3.js    82 kB       1  [emitted]  main
 ./vendor.26886caf15818fa82dfa.js    46 kB       2  [emitted]  vendor
./runtime.79f17c27b335abc7aaf4.js  1.45 kB       3  [emitted]  runtime
   [0] ./index.js 29 kB {1} [built]
   [2] (webpack)/buildin/global.js 488 bytes {2} [built]
   [3] (webpack)/buildin/module.js 495 bytes {2} [built]
```

为了解决这种问题, 我们通过 `HashedModuleIdsPlugin` 这个内置插件来计算并生成我们的 module id

```bash
$ webpack
Hash: df3474e4f76528e3bbc9
Version: webpack 3.8.1
Time: 2150ms
                           Asset      Size  Chunks             Chunk Names
      ./0.6168aaac8461862eab7a.js  22.5 kB       0  [emitted]
   ./main.a2e49a279552980e3b91.js    60 kB       1  [emitted]  main
 ./vendor.ff9f7ea865884e6a84c8.js    46 kB       2  [emitted]  vendor
./runtime.25f5d0204e4f77fa57a1.js  1.45 kB       3  [emitted]  runtime
[3IRH] ./index.js 29 kB {1} [built]
[DuR2] (webpack)/buildin/global.js 488 bytes {2} [built]
[JkW7] (webpack)/buildin/module.js 495 bytes {2} [built]
[LbCc] ./webPlayer.js 24 kB {1} [built]
[lebJ] ./comments.js 58 kB {0} [built]
[02Tr] ./ads.js 74 kB {1} [built]
    + 1 hidden module
```

```js
// webpack.config.js
module.exports = {
  plugins: [new webpack.HashedModuleIdsPlugin()],
}
```

#### 3.3 监控并分析你的 Web 应用文件

```js
// webpack.config.js
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin

module.exports = {
  plugins: [new BundleAnalyzerPlugin()],
}
```

![](https://blog-crown-im-1256172570.file.myqcloud.com/7jy8m.jpg)

---

### 4. `Vue cli ^3.0.0` 中的实践
